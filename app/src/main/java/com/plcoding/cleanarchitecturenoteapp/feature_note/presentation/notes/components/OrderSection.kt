package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.notes.components

import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.util.NoteOrder
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.util.OrderType

@Composable
fun OrderSection(
    modifier: Modifier = Modifier
    ,noteOrder: NoteOrder = NoteOrder.Date(OrderType.Descending)
    ,onChangedOrder: (NoteOrder) -> Unit
){
    Column(
        modifier = modifier
    ) {
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            DefaultRadioButton(
                text = "Title"
                , selected = noteOrder is NoteOrder.Title
                , onClick = { onChangedOrder(NoteOrder.Title(noteOrder.orderType)) }
            )
            Spacer(modifier = Modifier.width(8.dp))
            DefaultRadioButton(
                text = "Date"
                , selected = noteOrder is NoteOrder.Date
                , onClick = { onChangedOrder(NoteOrder.Date(noteOrder.orderType)) }
            )
            Spacer(modifier = Modifier.width(8.dp))
            DefaultRadioButton(
                text = "Color"
                , selected = noteOrder is NoteOrder.Color
                , onClick = { onChangedOrder(NoteOrder.Color(noteOrder.orderType)) }
            )
            Spacer(modifier = Modifier.width(8.dp))
        }//Row
        Spacer(modifier = Modifier.height(16.dp))
        Row(Modifier.fillMaxWidth(), verticalAlignment = Alignment.CenterVertically) {
            DefaultRadioButton(
                text = "Ascending"
                , selected = noteOrder.orderType is OrderType.Ascending
                , onClick = { onChangedOrder(noteOrder.copy(OrderType.Ascending)) }
            )
            Spacer(modifier = Modifier.width(8.dp))
            DefaultRadioButton(
                text = "Descending"
                , selected = noteOrder.orderType is OrderType.Descending
                , onClick = { onChangedOrder(noteOrder.copy(OrderType.Descending)) }
            ) }//Row
    }
}