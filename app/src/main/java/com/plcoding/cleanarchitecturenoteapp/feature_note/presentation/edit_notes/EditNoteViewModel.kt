package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes.components

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.graphics.toArgb
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.model.InvalidNoteException
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.model.Note
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.use_case.NoteUseCases
import com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes.EditNoteEvents
import com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes.NoteTextFieldState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EditNoteViewModel @Inject constructor(
    private val noteUseCases: NoteUseCases
    ,savedStateHandle: SavedStateHandle
) : ViewModel(){
    private val _noteTitle = mutableStateOf(NoteTextFieldState(
        hint = "Ingresa el titulo.."
    ))
    val noteTitle: State<NoteTextFieldState> = _noteTitle

    private val _noteContent = mutableStateOf(NoteTextFieldState(
        hint = "Ingresa el contenido de la nota.."
    ))
    val noteContent: State<NoteTextFieldState> = _noteContent

    private val _colorNote = mutableStateOf<Int>(Note.noteColor.random().toArgb())
    val colorNote: State<Int> = _colorNote

    private val _eventFlow = MutableSharedFlow<UIEvents>()
    val eventFlow: SharedFlow<UIEvents> = _eventFlow

    private var currentNoteId: Int? = null

    sealed class UIEvents{
        data class ShowSnackBar(val message: String): UIEvents()
        object SaveNote: UIEvents()
    }

    init{
        savedStateHandle.get<Int>("noteId")?.let { noteId ->
            if(noteId != -1){
                viewModelScope.launch {
                    noteUseCases.getNote(noteId)?.also { note ->
                        currentNoteId = noteId
                        _noteTitle.value = noteTitle.value.copy(
                            text = note.title
                            , isHintVisible = false
                        )
                        _noteContent.value = noteContent.value.copy(
                            text = note.content
                            , isHintVisible = false
                        )
                        _colorNote.value = note.color

                     }
                }

            }
        }
    }

    fun onEvent(events: EditNoteEvents){
        when(events){
            is EditNoteEvents.EnteredTitle -> {
                _noteTitle.value = noteTitle.value.copy(
                    text = events.value
                )
            }
            is EditNoteEvents.ChangedTitleFocus -> {
                _noteTitle.value = noteTitle.value.copy(
                    isHintVisible = !events.focusState.isFocused && noteTitle.value.text.isBlank()
                )
            }
            is EditNoteEvents.EnteredContent -> {
                _noteContent.value = noteContent.value.copy(
                    text = events.value
                )
            }
            is EditNoteEvents.ChangedContentFocus -> {
                _noteContent.value = noteContent.value.copy(
                    isHintVisible = !events.focusState.isFocused && noteContent.value.text.isBlank()
                )
            }
            is EditNoteEvents.ChangedColor -> {
                _colorNote.value = events.color
            }
            EditNoteEvents.SaveNote -> {
                viewModelScope.launch {
                    try {
                        noteUseCases.addNote(
                            Note(
                                title = noteTitle.value.text
                                , content = noteContent.value.text
                                , timestamp = System.currentTimeMillis()
                                , color = colorNote.value
                                , id = currentNoteId
                            )
                        )
                        _eventFlow.emit(UIEvents.SaveNote )
                    }catch (ex: InvalidNoteException){
                        _eventFlow.emit(
                            UIEvents.ShowSnackBar(
                                ex.message ?: "No se pudo guardar la nota"
                            )
                        )
                    }
                }//launch
            }
        }//when
    }
}