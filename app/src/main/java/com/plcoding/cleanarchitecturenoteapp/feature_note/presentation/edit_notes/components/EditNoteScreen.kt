package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes.components

import androidx.compose.animation.Animatable
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Save
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.plcoding.cleanarchitecturenoteapp.feature_note.domain.model.Note
import com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes.EditNoteEvents
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@Composable
fun EditNoteScreen(
    navHostController: NavHostController
    ,editNoteViewModel: EditNoteViewModel = hiltViewModel()
    ,noteColor: Int
){
    val titleState = editNoteViewModel.noteTitle.value
    val contentState = editNoteViewModel.noteContent.value

    val scaffoldState = rememberScaffoldState()
    val coroutine = rememberCoroutineScope()

    val noteBackgroundAnimatable = remember{
        Animatable(
            Color(if(noteColor != -1) noteColor else editNoteViewModel.colorNote.value)
        )
    }

    LaunchedEffect(key1 = true){
        editNoteViewModel.eventFlow.collectLatest { event ->
            when(event){
                is EditNoteViewModel.UIEvents.ShowSnackBar -> {
                    scaffoldState.snackbarHostState.showSnackbar( event.message )
                }
                is EditNoteViewModel.UIEvents.SaveNote -> {
                    navHostController.navigateUp()
                }
                else -> {}
            }
        }
    }

    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                onClick = { editNoteViewModel.onEvent(EditNoteEvents.SaveNote) }
            ) {
                Icon(imageVector = Icons.Default.Save, contentDescription = "save note")
            }
        }
        , scaffoldState = scaffoldState
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(noteBackgroundAnimatable.value)
                .padding(16.dp)
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
                , horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Note.noteColor.forEach { color ->
                    val colorInt = color.toArgb()
                    Box(
                        modifier = Modifier
                            .size(50.dp)
                            .shadow(15.dp, CircleShape)
                            .clip(CircleShape)
                            .background(color)
                            .border(
                                width = 3.dp,
                                color = if (editNoteViewModel.colorNote.value == colorInt) {
                                    Color.Black
                                } else {
                                    Color.Transparent
                                },
                                shape = CircleShape
                            )
                            .clickable {
                                coroutine.launch {
                                    noteBackgroundAnimatable.animateTo(
                                        targetValue = Color(colorInt), animationSpec = tween(500)
                                    )
                                    editNoteViewModel.onEvent(EditNoteEvents.ChangedColor(colorInt))
                                }//coroutine
                            }//Clickable
                    )//Box
                }
            }
            Spacer(modifier = Modifier.height(16.dp))
            TransparentHintTextField(
                text = titleState.text
                , hint = titleState.hint
                , onValueChanged = { editNoteViewModel.onEvent(EditNoteEvents.EnteredTitle(it)) }
                , onFocusChanged = { editNoteViewModel.onEvent(EditNoteEvents.ChangedTitleFocus(it)) }
                , isVisibleHint = titleState.isHintVisible
                , singleLine = true
                , textStyle = MaterialTheme.typography.h5
            )
            Spacer(modifier = Modifier.height(16.dp))
            TransparentHintTextField(
                text = contentState.text
                , hint = contentState.hint
                , onValueChanged = { editNoteViewModel.onEvent(EditNoteEvents.EnteredContent(it)) }
                , onFocusChanged = { editNoteViewModel.onEvent(EditNoteEvents.ChangedContentFocus(it)) }
                , isVisibleHint = contentState.isHintVisible
                , textStyle = MaterialTheme.typography.body1
                , modifier = Modifier.fillMaxHeight()
            )
        }
    }
}