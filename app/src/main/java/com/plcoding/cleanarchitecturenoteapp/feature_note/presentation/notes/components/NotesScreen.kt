package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.notes.components

import androidx.compose.animation.*
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Sort
import androidx.compose.runtime.Composable
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.notes.NotesEvents
import com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.util.Screen
import kotlinx.coroutines.launch

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun NotesScreen(
    noteViewModel: NotesViewModel = hiltViewModel()
    ,navHostController: NavHostController
){
    val state = noteViewModel.state.value
    val scaffoldState = rememberScaffoldState()
    val coroutine = rememberCoroutineScope()
    
    Scaffold(
        floatingActionButton = {
            FloatingActionButton(
                onClick = { navHostController.navigate(Screen.EditNoteScreen.route) }
            ) {
                Icon(imageVector = Icons.Default.Add, contentDescription = "Add Note")
            }
        } //floatingActionButton
        , scaffoldState = scaffoldState
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp)
        ) {
            Row(
                modifier = Modifier.fillMaxWidth()
                , horizontalArrangement = Arrangement.SpaceBetween
                , verticalAlignment = Alignment.CenterVertically
            ) {
                Text(text = "Your Notes", style = MaterialTheme.typography.h4)
                IconButton(onClick = { noteViewModel.onEvent(NotesEvents.ToggleOrderSection) }) {
                    Icon(imageVector = Icons.Default.Sort, contentDescription = "Toggle Order Section ")
                }
            }//Row
            AnimatedVisibility(
                visible = state.isOrderSectionVisible
                , enter = fadeIn() + slideInVertically()
                , exit = fadeOut() + slideOutVertically()
            ) {
                OrderSection(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 16.dp)
                    , noteOrder = state.noteOrder
                    , onChangedOrder = { newOrder ->
                        noteViewModel.onEvent(NotesEvents.Order(newOrder))
                    }
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            LazyColumn(Modifier.fillMaxSize()){
                items(state.notes){ note ->
                    NoteItem(
                        note = note
                        , modifier = Modifier.fillMaxWidth()
                            .clickable {
                               navHostController.navigate(Screen.EditNoteScreen.route +
                                       "?noteId=${note.id}&noteColor=${note.color}"
                               )
                            }
                        , onDelete = {
                            noteViewModel.onEvent(NotesEvents.DeleteNote(note))
                            coroutine.launch {
                                val result = scaffoldState.snackbarHostState.showSnackbar(
                                    message = "Nota Eliminada"
                                    , actionLabel = "Deshacer"
                                )
                                if(result == SnackbarResult.ActionPerformed){
                                    noteViewModel.onEvent(NotesEvents.RestoreNote)
                                }
                            }
                        }
                    )//NoteItem
                    Spacer(modifier = Modifier.height(16.dp))
                }//item
            }
        }//Column
    }
}