package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.util

sealed class Screen(val route: String){
    object NotesScreen: Screen("NotesScreen")
    object EditNoteScreen: Screen("EditNoteScreen")
}
