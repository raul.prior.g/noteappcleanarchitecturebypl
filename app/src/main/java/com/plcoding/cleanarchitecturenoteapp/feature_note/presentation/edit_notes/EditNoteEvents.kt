package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.edit_notes

import androidx.compose.ui.focus.FocusState

sealed class EditNoteEvents{
    data class EnteredTitle(val value: String): EditNoteEvents()
    data class ChangedTitleFocus(val focusState: FocusState): EditNoteEvents()
    data class EnteredContent(val value: String): EditNoteEvents()
    data class ChangedContentFocus(val focusState: FocusState): EditNoteEvents()
    data class ChangedColor(val color: Int): EditNoteEvents()
    object SaveNote: EditNoteEvents()
}
