package com.plcoding.cleanarchitecturenoteapp.feature_note.presentation.notes.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.MaterialTheme
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun DefaultRadioButton(
    text: String
    ,selected: Boolean
    ,onClick: () -> Unit
    ,modifier: Modifier = Modifier
){
    RadioButton(
        selected = selected
        , onClick = onClick
        ,colors = RadioButtonDefaults.colors(
            selectedColor = MaterialTheme.colors.primary
            , unselectedColor = MaterialTheme.colors.onBackground
        )
    )
    Spacer(modifier = Modifier.width(8.dp))
    Text(text = text, style = MaterialTheme.typography.body1)
}